#!/usr/bin/env bash

PRAW_FILE="praw.ini"

echo "[daily_scramble_bot]" > ${PRAW_FILE}
echo "client_id=${BOT_CLIENT_ID}" >> ${PRAW_FILE}
echo "client_secret=${BOT_CLIENT_SECRET}" >> ${PRAW_FILE}
echo "username=${BOT_USERNAME}" >> ${PRAW_FILE}
echo "password=${BOT_PASSWORD}" >> ${PRAW_FILE}
echo "user_agent=${BOT_USER_AGENT}" >> ${PRAW_FILE}
