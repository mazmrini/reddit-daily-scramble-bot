from typing import Dict, Callable
import sys
import praw

from bot.bot import Bot
from bot.scramblers.three_by_three_scrambler import ThreeByThreeScrambler
from bot.scramblers.square_one_scrambler import SquareOneScrambler
from bot.links.link_builder import LinkBuilder
from bot.links.alg_cubing_net_link_builder import AlgCubingNetLinkBuilder
from bot.links.cube_db_link_builder import CubeDbLinkBuilder
from bot.links.composite_link_builder import CompositeLinkBuilder
from bot.threads.thread import Thread
from bot.threads.cubers_daily_thread import CubersDailyThread
from bot.threads.print_thread import PrintThread
from bot.scramble_comment_formatter import ScrambleCommentFormatter
from bot.quotes import quotes


def exit_with_error_message() -> None:
    print("\nError, program syntax:\n\tpython bot/ <reddit|print>\n")
    sys.exit(-1)


if len(sys.argv) != 2:
    exit_with_error_message()


threads_opt: Dict[str, Callable[[], Thread]] = {
    "reddit": lambda: CubersDailyThread(praw.Reddit("daily_scramble_bot")),
    "print": lambda: PrintThread()
}


thread_arg = sys.argv[-1]
if thread_arg not in threads_opt:
    exit_with_error_message()


thread = threads_opt[thread_arg]()
scramblers = [SquareOneScrambler(), ThreeByThreeScrambler()]
link_builder = CompositeLinkBuilder([AlgCubingNetLinkBuilder(), CubeDbLinkBuilder()])
scramble_comment_formatter = ScrambleCommentFormatter(quotes, link_builder)

bot = Bot(thread, scramblers, scramble_comment_formatter)
bot.comment()

print("Done!")
