import unittest
from mockito import mock, when, unstub

from bot.scramble_comment_formatter import ScrambleCommentFormatter
from bot.scramblers.scrambler import Scramble
from bot.links.link_builder import LinkBuilder


class ScrambleCommentFormatterTests(unittest.TestCase):
    SETUP_3X3 = "R2 U B2 L2 U B2 L2 B2 D2 B2 L' B"
    SETUP_2X2 = "U U' U U'"

    ALG_3X3_LINK = f"3x3 - ALG_3X3_RECONSTRUCTION_LINK"
    ALG_2X2_LINK = f"2x2 - ALG_3X3_RECONSTRUCTION_LINK"

    QUOTE = "QUOTE"

    def setUp(self):
        self.scrambles = [Scramble.for_3x3(self.SETUP_3X3), Scramble.for_2x2(self.SETUP_2X2)]
        link_builder = mock(LinkBuilder)
        when(link_builder).build(self.scrambles[0]).thenReturn(self.ALG_3X3_LINK)
        when(link_builder).build(self.scrambles[1]).thenReturn(self.ALG_2X2_LINK)

        self.scramble_comment_formatter = ScrambleCommentFormatter([self.QUOTE,
                                                                    self.QUOTE,
                                                                    self.QUOTE], link_builder)

    def tearDown(self):
        unstub()

    def test_given_a_scramble_when_formatting_then_comment_contains_scrambles_setup_in_bold(self):
        result = self.scramble_comment_formatter.format(self.scrambles)

        self.assertIn(f"**{self.SETUP_3X3}**", result)
        self.assertIn(f"**{self.SETUP_2X2}**", result)

    def test_given_a_scramble_when_formatting_then_comment_contains_git_url(self):
        result = self.scramble_comment_formatter.format(self.scrambles)

        self.assertIn(ScrambleCommentFormatter.git_url, result)

    def test_given_a_scramble_when_formatting_then_comment_contains_the_quote(self):
        result = self.scramble_comment_formatter.format(self.scrambles)

        self.assertIn(self.QUOTE, result)

    def test_given_a_scramble_when_formatting_then_comment_contains_reconstruction_link(self):
        result = self.scramble_comment_formatter.format(self.scrambles)

        self.assertIn(self.ALG_3X3_LINK, result)
        self.assertIn(self.ALG_2X2_LINK, result)
