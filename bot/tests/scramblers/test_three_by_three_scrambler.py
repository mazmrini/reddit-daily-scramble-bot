import unittest
from mockito import when, unstub
from bot.scramblers import scrambler_333
from bot.scramblers.three_by_three_scrambler import ThreeByThreeScrambler


class ThreeByThreeScramblerTests(unittest.TestCase):
    SETUP = "SOME SETUP"

    def setUp(self) -> None:
        self.scrambler = ThreeByThreeScrambler()

    def tearDown(self) -> None:
        unstub()

    def test_given_scramble_to_strip_when_scrambling_then_returns_expected_stripped_scramble(self) -> None:
        scramble_to_strip = f"  {self.SETUP}  "
        when(scrambler_333).call("scramble_333.getRandomScramble").thenReturn(scramble_to_strip)

        result = self.scrambler.scramble()

        self.assertEqual("3x3", result.cube_name)
        self.assertEqual(3, result.cube_size)
        self.assertEqual(self.SETUP, result.setup)

    def test_given_no_mocks_when_scrambling_then_returns_string_with_more_than_10_chars(self) -> None:
        scramble = self.scrambler.scramble()

        result = len(scramble.setup)

        self.assertGreater(result, 10, f"scramble: [{scramble.setup}]")
