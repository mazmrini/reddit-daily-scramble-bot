import unittest
from mockito import when, unstub
from bot.scramblers import square_1
from bot.scramblers.square_one_scrambler import SquareOneScrambler


class SquareOnecramblerTests(unittest.TestCase):
    SETUP = "SOME SETUP"

    def setUp(self) -> None:
        self.scrambler = SquareOneScrambler()

    def tearDown(self) -> None:
        unstub()

    def test_given_scramble_to_strip_when_scrambling_then_returns_expected_stripped_scramble(self) -> None:
        scramble_to_strip = f"  {self.SETUP}  "
        when(square_1).call("sql_scrambler.getRandomScramble").thenReturn(scramble_to_strip)

        result = self.scrambler.scramble()

        self.assertEqual("Square-1", result.cube_name)
        self.assertEqual(self.SETUP, result.setup)

    def test_given_scramble_with_slashes_when_scrambling_then_returns_readable_output(self) -> None:
        expected = "(4,-3) / (5,0) / (-3,2) /"
        scramble_to_strip = "(4, -3)/(5, 0)/(-3, 2)/"
        when(square_1).call("sql_scrambler.getRandomScramble").thenReturn(scramble_to_strip)

        result = self.scrambler.scramble()

        self.assertEqual(expected, result.setup)

    def test_given_no_mocks_when_scrambling_then_returns_string_with_more_than_10_chars(self) -> None:
        scramble = self.scrambler.scramble()

        result = len(scramble.setup)

        self.assertGreater(result, 10, f"scramble: [{scramble.setup}]")
