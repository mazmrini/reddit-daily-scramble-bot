import unittest
from bot.scramblers.scrambler import Scramble
from bot.links.link_builder import UnsupportedScrambleError
from bot.links.alg_cubing_net_link_builder import AlgCubingNetLinkBuilder


class AlgCubingNetLinkBuilderTests(unittest.TestCase):
    SETUP_3X3 = "R2 U B2 L2 U B2 L2 B2 D2 B2 L' B"
    SETUP_2X2 = "U U' U U'"

    ALG_3X3_RECONSTRUCTION_URL = "https://alg.cubing.net/?puzzle=3x3x3&setup=R2_U_B2_L2_U_B2_L2_B2_D2_B2_L-_B"
    ALG_2X2_RECONSTRUCTION_URL = "https://alg.cubing.net/?puzzle=2x2x2&setup=U_U-_U_U-"

    def setUp(self):
        self.alg_cubing_net_link_builder = AlgCubingNetLinkBuilder()

    def test_given_3x3_scramble_when_building_link_then_matches_expected_link(self):
        expected = f"[alg.cubing.net]({self.ALG_3X3_RECONSTRUCTION_URL})"

        result = self.alg_cubing_net_link_builder.build(Scramble.for_3x3(self.SETUP_3X3))

        self.assertEqual(expected, result)

    def test_given_2x2_scramble_when_building_link_then_matches_expected_link(self):
        expected = f"[alg.cubing.net]({self.ALG_2X2_RECONSTRUCTION_URL})"

        result = self.alg_cubing_net_link_builder.build(Scramble.for_2x2(self.SETUP_2X2))

        self.assertEqual(expected, result)

    def test_given_square_1_scramble_when_building_link_then_raises_error(self):
        with self.assertRaises(UnsupportedScrambleError):
            self.alg_cubing_net_link_builder.build(Scramble.for_square_1("some_url"))
