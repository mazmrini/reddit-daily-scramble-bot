import unittest
from mockito import mock, when, unstub

from bot.scramblers.scrambler import Scramble
from bot.links.link_builder import LinkBuilder, UnsupportedScrambleError
from bot.links.composite_link_builder import CompositeLinkBuilder


class CompositeBuilderTests(unittest.TestCase):
    BUILDER_1_LINK = "BUILDER_1_LINK"
    BUILDER_3_LINK = "BUILDER_3_LINK"

    COMPOSITE_BUILDER_LINK = "BUILDER_1_LINK | BUILDER_3_LINK"

    def setUp(self):
        self.scramble = Scramble.for_3x3("whatever")
        builder_1 = mock(LinkBuilder)
        self.failed_builder_2 = mock(LinkBuilder)
        builder_3 = mock(LinkBuilder)
        when(builder_1).build(self.scramble).thenReturn(self.BUILDER_1_LINK)
        when(self.failed_builder_2).build(self.scramble).thenRaise(UnsupportedScrambleError)
        when(builder_3).build(self.scramble).thenReturn(self.BUILDER_3_LINK)

        self.composite_link_builder = CompositeLinkBuilder([builder_1, self.failed_builder_2, builder_3])

    def tearDown(self):
        unstub()

    def test_given_scramble_when_building_link_then_matches_expected_link(self):
        result = self.composite_link_builder.build(self.scramble)

        self.assertEqual(self.COMPOSITE_BUILDER_LINK, result)

    def test_given_only_failing_link_builders_when_building_link_then_return_empty_string(self):
        self.composite_link_builder = CompositeLinkBuilder([self.failed_builder_2, self.failed_builder_2])

        result = self.composite_link_builder.build(self.scramble)

        self.assertEqual("", result)