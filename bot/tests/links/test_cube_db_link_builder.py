import unittest
from bot.scramblers.scrambler import Scramble
from bot.links.cube_db_link_builder import CubeDbLinkBuilder


class CubeDbLinkBuilderTests(unittest.TestCase):
    SETUP_3X3 = "R2 U B2 L2 U' B2 L2 B2 D2 B2 L' B"
    SETUP_2X2 = "F' R' F2 U2 F' U F2 U F' U'"
    SETUP_SQ1 = "(1,0) / (-3,0) / (5,-4) / (4,-5) / (-3,0) / (0,-3) / (6,-1) / (-3,0) / (6,-3) / (-3,0)"

    ALG_3X3_RECONSTRUCTION_URL = ("https://www.cubedb.net/?puzzle=3&title=Your_Daily_Scramble&scramble="
                                  "R2_U_B2_L2_U-_B2_L2_B2_D2_B2_L-_B")
    ALG_2X2_RECONSTRUCTION_URL = ("https://www.cubedb.net/?puzzle=2&title=Your_Daily_Scramble&scramble="
                                  "F-_R-_F2_U2_F-_U_F2_U_F-_U-")
    ALG_SQ1_RECONSTRUCTION_URL = ("https://www.cubedb.net/?puzzle=SQ1&title=Your_Daily_Scramble&scramble=%25281%2C0"
                                  "%2529_%2F_%2528%252D3%2C0%2529_%2F_%25285%2C%252D4%2529_%2F_%25284%2C%252D5%2529_"
                                  "%2F_%2528%252D3%2C0%2529_%2F_%25280%2C%252D3%2529_%2F_%25286%2C%252D1%2529_%2F_"
                                  "%2528%252D3%2C0%2529_%2F_%25286%2C%252D3%2529_%2F_%2528%252D3%2C0%2529")

    def setUp(self):
        self.cube_db_link_builder = CubeDbLinkBuilder()

    def test_given_3x3_scramble_when_building_link_then_matches_expected_link(self):
        expected = f"[cubedb.net]({self.ALG_3X3_RECONSTRUCTION_URL})"

        result = self.cube_db_link_builder.build(Scramble.for_3x3(self.SETUP_3X3))

        self.assertEqual(expected, result)

    def test_given_2x2_scramble_when_building_link_then_matches_expected_link(self):
        expected = f"[cubedb.net]({self.ALG_2X2_RECONSTRUCTION_URL})"

        result = self.cube_db_link_builder.build(Scramble.for_2x2(self.SETUP_2X2))

        self.assertEqual(expected, result)

    def test_given_square_1_scramble_when_building_link_then_matches_expected_link(self):
        expected = f"[cubedb.net]({self.ALG_SQ1_RECONSTRUCTION_URL})"

        result = self.cube_db_link_builder.build(Scramble.for_square_1(self.SETUP_SQ1))

        self.assertEqual(expected, result)
