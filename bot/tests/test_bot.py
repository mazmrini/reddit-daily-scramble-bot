import unittest
from mockito import mock, when, unstub, ANY, verify

from bot.threads.thread import Thread
from bot.scramblers.scrambler import Scrambler, Scramble
from bot.scramble_comment_formatter import ScrambleCommentFormatter
from bot.bot import Bot


class BotTests(unittest.TestCase):
    FORMATTED_COMMENT = "I COMMENT THE SCRAMBLES"
    INVALID_SUBMISSION_TITLE = "INVALID_SUBMISSION_TITLE"
    VALID_SUBMISSION_TITLE = "Daily Discussion Thread - Apr, 09 2019"

    def setUp(self):
        self.thread = mock(Thread, strict=False)

        first_scrambler = mock(Scrambler)
        second_scrambler = mock(Scrambler)
        scramble_comment_formatter = mock(ScrambleCommentFormatter)

        scrambles = [Scramble.for_2x2("2x2 scramble"), Scramble.for_3x3("3x3 scramble")]
        when(first_scrambler).scramble().thenReturn(scrambles[0])
        when(second_scrambler).scramble().thenReturn(scrambles[1])
        when(scramble_comment_formatter).format(scrambles).thenReturn(self.FORMATTED_COMMENT)

        self.bot = Bot(self.thread, [first_scrambler, second_scrambler], scramble_comment_formatter)

    def tearDown(self):
        unstub()

    def test_given_thread_comment_works_when_commenting_then_comment_is_made(self):
        self.bot.comment()

        verify(self.thread).comment(self.FORMATTED_COMMENT)

    def test_given_thread_comment_fails_when_commenting_then_raise_exception(self):
        when(self.thread).comment(ANY(str)).thenRaise(TypeError())

        with self.assertRaises(TypeError) as _:
            self.bot.comment()


