import unittest
from mockito import mock, unstub, ANY, when, verify, never

from praw.models import Comment
from praw.models.reddit.submission import Submission
from praw.models.reddit.subreddit import Subreddit

from bot.threads.cubers_daily_thread import CubersDailyThread, DailyDiscussionThreadNotFound


class CubersDailyThreadTests(unittest.TestCase):
    SCRAMBLE = "SOME_SCRAMBLE"
    COMMENT = "I COMMENT THE SCRAMBLES"
    INVALID_SUBMISSION_TITLE = "INVALID_SUBMISSION_TITLE"
    VALID_SUBMISSION_TITLE = "Daily Discussion Thread - Apr, 09 2019"

    def setUp(self):
        reddit = mock(strict=False)
        self.subreddit = mock(Subreddit, strict=False)
        self.valid_submission = mock(Submission)
        self.invalid_submission = mock(Submission)
        self.comment = mock(Comment, strict=False)

        self.valid_submission.title = self.VALID_SUBMISSION_TITLE
        self.invalid_submission.title = self.INVALID_SUBMISSION_TITLE

        when(reddit).subreddit(CubersDailyThread.subreddit).thenReturn(self.subreddit)
        when(self.subreddit).sticky(number=ANY(int)).thenReturn(self.valid_submission)
        when(self.valid_submission).reply(self.COMMENT).thenReturn(self.comment)

        self.cubers_daily_thread = CubersDailyThread(reddit)

    def tearDown(self):
        unstub()

    def test_given_daily_discussion_thread_exists_when_commenting_then_comment_is_made(self):
        self.cubers_daily_thread.comment(self.COMMENT)

        verify(self.valid_submission).reply(self.COMMENT)
        verify(self.comment).disable_inbox_replies()

    def test_given_no_stickies_when_commenting_then_raises_error_and_dont_comment(self):
        self.__given_no_stickies()

        with self.assertRaises(DailyDiscussionThreadNotFound) as _:
            self.cubers_daily_thread.comment(self.COMMENT)

        self.__verify_dont_comment()

    def test_given_no_daily_thread_found_when_commenting_then_raises_error_and_dont_comment(self):
        self.__given_stickies_but_no_daily_discussion_thread()

        with self.assertRaises(DailyDiscussionThreadNotFound) as _:
            self.cubers_daily_thread.comment(self.COMMENT)

        verify(self.subreddit, times=CubersDailyThread.nb_stickies_tries).sticky(number=ANY(int))
        self.__verify_dont_comment()

    def __given_no_stickies(self):
        when(self.subreddit).sticky(number=ANY(int)).thenRaise(Exception)

    def __given_stickies_but_no_daily_discussion_thread(self):
        when(self.subreddit).sticky(number=ANY(int)).thenReturn(self.invalid_submission)

    def __verify_dont_comment(self):
        verify(self.valid_submission, times=never).reply(ANY(str))
        verify(self.invalid_submission, times=never).reply(ANY(str))
