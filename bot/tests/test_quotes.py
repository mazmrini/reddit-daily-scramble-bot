import unittest

from bot.quotes import quotes


class QuotesTests(unittest.TestCase):
    def test_all_quotes_ends_with_a_dot(self):
        for quote in quotes:
            last_char = quote[-1]

            self.assertEqual(".", last_char, f"Quote [{quote}]")

    def test_none_of_the_quotes_are_empty(self):
        for quote in quotes:
            length = len(quote)

            self.assertGreater(length, 0)
