from bot.scramblers.scrambler import Scramble
from bot.links.link_builder import LinkBuilder, UnsupportedScrambleError


class AlgCubingNetLinkBuilder(LinkBuilder):
    def build(self, scramble: Scramble) -> str:
        if scramble.cube_name == Scramble.for_square_1("").cube_name:
            raise UnsupportedScrambleError()

        puzzle = f"{scramble.cube_size}x{scramble.cube_size}x{scramble.cube_size}"
        link_setup = scramble.setup.replace(" ", "_").replace("'", "-")

        return f"[alg.cubing.net](https://alg.cubing.net/?puzzle={puzzle}&setup={link_setup})"
