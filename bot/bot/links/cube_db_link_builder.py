from bot.scramblers.scrambler import Scramble
from bot.links.link_builder import LinkBuilder, UnsupportedScrambleError


class CubeDbLinkBuilder(LinkBuilder):
    __ENCODE_MAP = {
        " ": "_",
        "'": "-",
        ",": "%2C",
        "/": "%2F",
        "-": "%252D",
        "(": "%2528",
        ")": "%2529"
    }

    def build(self, scramble: Scramble) -> str:
        encoded_scramble = self.__encode_setup(scramble.setup)
        puzzle = self.__puzzle(scramble)
        link = f"https://www.cubedb.net/?puzzle={puzzle}&title=Your_Daily_Scramble&scramble={encoded_scramble}"

        return f"[cubedb.net]({link})"

    def __puzzle(self, scramble: Scramble) -> str:
        if Scramble.for_square_1("whatever").cube_name == scramble.cube_name:
            return "SQ1"
        elif Scramble.for_2x2("whatever").cube_name == scramble.cube_name:
            return "2"

        return "3"

    def __encode_setup(self, setup: str) -> str:
        result = ""
        for char in setup:
            result += self.__ENCODE_MAP.get(char, char)

        return result
