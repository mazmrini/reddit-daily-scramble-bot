import abc

from bot.scramblers.scrambler import Scramble


class LinkBuilder(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def build(self, scramble: Scramble) -> str:
        pass


class UnsupportedScrambleError(Exception):
    pass
