from typing import List

from bot.scramblers.scrambler import Scramble
from bot.links.link_builder import LinkBuilder, UnsupportedScrambleError


class CompositeLinkBuilder(LinkBuilder):
    def __init__(self, builders: List[LinkBuilder]) -> None:
        self.builders = builders

    def build(self, scramble: Scramble) -> str:
        links = []
        for builder in self.builders:
            try:
                links.append(builder.build(scramble))
            except UnsupportedScrambleError as e:
                pass

        return " | ".join(links)
