import execjs
from os import path

current_dir = path.dirname(path.realpath(__file__))

with open(path.join(current_dir, 'js/mathlib.js'), 'r') as f:
    MATHLIB_SRC = f.read()

with open(path.join(current_dir, 'js/cross.js'), 'r') as f:
    CROSS_SRC = f.read()

with open(path.join(current_dir, 'js/scramble.js'), 'r') as f:
    SCRAMBLE_SRC = f.read()

with open(path.join(current_dir, 'js/scramble_222.js'), 'r') as f:
    SCRAMBLE_222_SRC = f.read()

with open(path.join(current_dir, 'js/scramble_333.js'), 'r') as f:
    SCRAMBLE_333_SRC = f.read()

with open(path.join(current_dir, 'js/square_1.js'), 'r') as f:
    SQUARE_1_SRC = f.read()

scrambler_222 = execjs.compile(MATHLIB_SRC + SCRAMBLE_222_SRC)
scrambler_333 = execjs.compile(MATHLIB_SRC + CROSS_SRC + SCRAMBLE_SRC + SCRAMBLE_333_SRC)
square_1 = execjs.compile(MATHLIB_SRC + SCRAMBLE_SRC + SQUARE_1_SRC)
