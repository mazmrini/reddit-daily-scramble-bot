from bot.scramblers import scrambler_222
from bot.scramblers.scrambler import Scrambler, Scramble


class TwoByTwoScrambler(Scrambler):
    method_call = "scramble_222.getRandomScramble"

    def scramble(self) -> Scramble:
        scramble = str(scrambler_222.call(self.method_call))

        return Scramble.for_2x2(scramble.strip())
