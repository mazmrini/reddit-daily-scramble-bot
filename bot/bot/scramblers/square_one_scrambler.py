from bot.scramblers import square_1
from bot.scramblers.scrambler import Scrambler, Scramble


class SquareOneScrambler(Scrambler):
    method_call = "sql_scrambler.getRandomScramble"

    def scramble(self) -> Scramble:
        scramble = str(square_1.call(self.method_call))

        return Scramble.for_square_1(self._clean_scramble(scramble))

    def _clean_scramble(self, scramble: str) -> str:
        return scramble.replace(", ", ",").replace("/", " / ").strip()
