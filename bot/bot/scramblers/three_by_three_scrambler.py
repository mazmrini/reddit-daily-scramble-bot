from bot.scramblers import scrambler_333
from bot.scramblers.scrambler import Scrambler, Scramble


class ThreeByThreeScrambler(Scrambler):
    method_call = "scramble_333.getRandomScramble"

    def scramble(self) -> Scramble:
        scramble = str(scrambler_333.call(self.method_call))

        return Scramble.for_3x3(scramble.strip())
