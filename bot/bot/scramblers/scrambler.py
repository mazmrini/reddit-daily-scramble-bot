import abc


class Scramble:
    def __init__(self, cube_name: str, cube_size: int, setup: str) -> None:
        self.cube_name = cube_name
        self.cube_size = cube_size
        self.setup = setup

    @classmethod
    def for_2x2(cls, setup: str) -> 'Scramble':
        return Scramble("2x2", 2, setup)

    @classmethod
    def for_3x3(cls, setup: str) -> 'Scramble':
        return Scramble("3x3", 3, setup)

    @classmethod
    def for_square_1(cls, setup: str) -> 'Scramble':
        return Scramble("Square-1", 1, setup)


class Scrambler(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def scramble(self) -> Scramble:
        pass
