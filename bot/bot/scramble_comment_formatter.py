import random
from typing import List

from bot.scramblers.scrambler import Scramble
from bot.links.link_builder import LinkBuilder


class ScrambleCommentFormatter:
    git_url = "https://gitlab.com/mazmrini/reddit-daily-scramble-bot"

    def __init__(self, quotes: List[str], link_builder: LinkBuilder) -> None:
        self.quotes = quotes
        self.link_builder = link_builder

    def format(self, scrambles: List[Scramble]) -> str:
        quote = random.choice(self.quotes)

        header = f"BeepBop! {quote} Here are your daily scrambles:"
        body = self.__make_body(scrambles)
        footer = f"Source code: [GitLab]({self.git_url})"

        return f"{header}\n\n\n{body}\n{footer}"

    def __make_body(self, scrambles: List[Scramble]) -> str:
        scrambles_body = "\n\n\n".join(map(self.__make_scramble_with_reconstruction_link, scrambles))

        return f"{scrambles_body}\n\n\nHave a nice day!\n***"

    def __make_scramble_with_reconstruction_link(self, scramble: Scramble) -> str:
        reconstruction_link = f"{self.link_builder.build(scramble)}"
        scramble_title = f"{scramble.cube_name} - {reconstruction_link}"

        return f"{scramble_title}\n\n**{scramble.setup}**"
