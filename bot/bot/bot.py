from typing import List

from bot.scramblers.scrambler import Scrambler
from bot.scramble_comment_formatter import ScrambleCommentFormatter
from bot.threads.thread import Thread


class Bot:
    subreddit = "cubers"
    submission_title = "daily discussion thread"
    nb_stickies_tries = 5

    def __init__(self, thread: Thread, scramblers: List[Scrambler], comment_formatter: ScrambleCommentFormatter) -> None:
        self.__thread = thread
        self.__scramblers = scramblers
        self.__comment_formatter = comment_formatter

    def comment(self) -> None:
        scrambles = list(map(lambda s: s.scramble(), self.__scramblers))
        comment_text = self.__comment_formatter.format(scrambles)

        self.__thread.comment(comment_text)
