from praw.reddit import Reddit
from praw.models.reddit.submission import Submission
from praw.models.reddit.subreddit import Subreddit

from bot.threads.thread import Thread


class CubersDailyThread(Thread):
    subreddit = "cubers"
    submission_title = "daily discussion thread"
    nb_stickies_tries = 5

    def __init__(self, reddit: Reddit) -> None:
        self.__reddit = reddit

    def comment(self, comment_text: str) -> None:
        submission = self.__find_daily_discussion_thread()

        comment = submission.reply(comment_text)
        comment.disable_inbox_replies()

    def __find_daily_discussion_thread(self) -> Submission:
        cubers: Subreddit = self.__reddit.subreddit(self.subreddit)
        for i in range(1, self.nb_stickies_tries + 1):
            try:
                submission = cubers.sticky(number=i)
            except Exception:
                raise DailyDiscussionThreadNotFound()

            if self.__is_daily_discussion_thread(submission.title):
                return submission

        raise DailyDiscussionThreadNotFound()

    def __is_daily_discussion_thread(self, submission_title: str) -> bool:
        return submission_title.lower().startswith(self.submission_title)


class DailyDiscussionThreadNotFound(Exception):
    pass
