import abc


class Thread(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def comment(self, comment_text: str) -> None:
        pass
