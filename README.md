# Reddit DailyScrambleBot

Posts a random 3x3x3 scramble in every *Daily Discussion Thread* of [/r/cubers](http://reddit.com/r/cubers)

### Sections
* [Credits](#credits)
* [Prerequisites](#prerequisites)
* [Run locally](#run-locally)
* [Run tests](#run-tests)
---
#### Credits

Special thanks to [euphwes/pyTwistyScrambler](https://github.com/euphwes/pyTwistyScrambler) where I took some code for the scrambler.

---
#### Prerequisites
* Python 3.6+
* pip
* Node.js
---
#### Run locally

```bash
pip install -r requirements.txt

// create praw.ini file with valid values

# reddit: will comment in the thread
# print: will print out to the console
python bot <reddit|print>
```
 
---
#### Run tests

```bash
pip install -r requirements.txt
pip install -r test.requirements.txt

cd bot
python -m unittest
```

#### Run linting

```bash
pip install -r requirements.txt
pip install -r test.requirements.txt

mypy bot
```